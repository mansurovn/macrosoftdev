'use strict';

const gulp = require('gulp');
const del = require('del');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const stylus = require('gulp-stylus');
const gulpIf = require('gulp-if');
const notify = require('gulp-notify');
const combine = require('stream-combiner2').obj;
const cssnano = require('gulp-cssnano');
const autoprefixer = require('autoprefixer');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const cache = require('gulp-cached');
const rename = require('gulp-rename');
const fs = require('fs');
const normalize = require('normalize-path');
const camelCase = require('camel-case');
const pathParse = require('path-parse');
const merge = require('gulp-merge-json');
const sass = require('gulp-sass');
const header = require('gulp-header');
const concat = require('gulp-concat');

//переменная среды, в зависимости от нее делаем различные действия над задачами
//const isDev = (process.env.NODE_ENV === 'development');

const dirs = {
    project: {
        self: "public_html",
        frontend: {
            self: "public_html/local/frontend",
            local: "public_html/local/frontend/local",
            vendor: "public_html/local/frontend/vendor"
        },
    },

    source: {
        self: "assets",
        frontend: {
            self: "assets/frontend",
            local: "assets/frontend/local",
            vendor: "assets/frontend/vendor"
        },
    }
};

const filePaths = {
    copy: [
        `${dirs.source.frontend.self}/**/*`,
        `!${dirs.source.frontend.local}/**/*.scss`,
        `!${dirs.source.frontend.local}/**/*.js`
    ],
    styles: [`${dirs.source.frontend.local}/**/*.scss`, `!${dirs.source.frontend.local}/**/*_dev.scss`],
    js: [`${dirs.source.frontend.local}/**/*.js`]
};

//уникальный для каждого проекта
const domain = 'alpinedev.local';

//чистка директории
gulp.task('clean', function () {

    return del(dirs.project.frontend.self, {force: true});

});


gulp.task('copy', function () {

    return combine(
        gulp.src(filePaths.copy, {base: dirs.source.frontend.self}),
        gulp.dest(dirs.project.frontend.self)
    ).on('error', notify.onError());

});


gulp.task('styles:source', function () {

    return combine(
        gulp.src(filePaths.styles, {base: dirs.source.frontend.self, dot: true}),
        cache('styles:source'),
        header('@import "common/variables";@import "common/mixins";'),
        sass({includePaths: dirs.source.frontend.local}),
        postcss([autoprefixer()]),
        gulp.dest(dirs.project.frontend.self)
    ).on('error', notify.onError());
});


gulp.task('styles:build', function () {

    return combine(
        gulp.src(
            [
                `${dirs.project.frontend.local}/**/*.css`,
                `!${dirs.project.frontend.local}/**/*.min.css`
            ],
            {
                base: dirs.project.frontend.local,
                dot: true
            }
        ),
        cache('styles:build'),
        cssnano({
            zindex: false,
            autoprefixer: false,
            reduceIdents: false,
            discardUnused: {fontFace: false}
        }),
        rename({suffix: '.min'}),
        gulp.dest(dirs.project.frontend.local)
    ).on('error', notify.onError());
});


gulp.task("js:source", function () {

    return combine(
        gulp.src(filePaths.js, {base: dirs.source.frontend.self, dot: true}),
        cache('js:source'),

        //the place is for the future

        gulp.dest(dirs.project.frontend.self)
    ).on("error", notify.onError());

});


gulp.task("js:build", function () {

    return combine(
        gulp.src(
            [
                `${dirs.project.frontend.local}/**/*.js`,
                `!${dirs.project.frontend.local}/**/*.min.js`,
            ],
            {
                base: dirs.project.frontend.local,
                dot: true
            }
        ),
        cache('js:build'),
        uglify(),
        rename({suffix: '.min'}),
        gulp.dest(dirs.project.frontend.local)
    ).on("error", notify.onError());

});


gulp.task("styles", gulp.series('styles:source', 'styles:build'));
gulp.task("js", gulp.series('js:source', 'js:build'));


gulp.task('dependencies:local', function () {
    del(`${dirs.project.frontend.self}/frontend.json`, {force: true});

    return combine(
        gulp.src(`${dirs.project.frontend.local}/**/dep.json`),

        merge({
            fileName: 'frontend.json',

            edit: (parsedJson, file) => {

                let filePath, files, libName, libPath, pDir, jsonResult = {};
                pDir = dirs.project;
                filePath = pathParse(normalize(file.history[0]));
                filePath.relDir = filePath.dir.substr(filePath.dir.indexOf(pDir.frontend.local) + pDir.frontend.local.length);
                libPath = pDir.frontend.local.substr((pDir.self + "/").length) + filePath.relDir;
                libName = camelCase(filePath.relDir);

                files = fs.readdirSync(filePath.dir);

                jsonResult[libName] = {
                    css: files.filter(value => !value.match(/\.min.css$/) && value.match(/\.css$/)),
                    js: files.filter(value => !value.match(/\.min.js$/) && value.match(/\.js$/)),
                    rel: parsedJson.map(value => camelCase(value))
                };

                jsonResult[libName].css = jsonResult[libName].css.map(value => `/${libPath}/${value}`);
                jsonResult[libName].js = jsonResult[libName].js.map(value => `/${libPath}/${value}`);

                if (jsonResult[libName].js.length) {
                    jsonResult[libName].js = jsonResult[libName].js[0];
                } else {
                    delete jsonResult[libName].js;
                }

                return jsonResult;
            }
        }),


        gulp.dest(dirs.project.frontend.self)
    ).on("error", notify.onError());

});


gulp.task('dependencies:vendor', function () {

    let startObj = {};

    try {
        startObj = JSON.parse(fs.readFileSync(`${dirs.project.frontend.self}/frontend.json`));
    } catch ($e) {
    }

    return combine(
        gulp.src(`${dirs.project.frontend.vendor}/dep.json`),

        merge({
            startObj: startObj,
            fileName: 'frontend.json',

            edit: (parsedJson, file) => {

                if (parsedJson) {
                    let pDir, libPath;

                    pDir = dirs.project;
                    libPath = pDir.frontend.vendor.substr((pDir.self + "/").length);

                    for (const libName in parsedJson) {
                        if (!parsedJson.hasOwnProperty(libName)) continue;

                        if (parsedJson[libName].css && parsedJson[libName].css.length) {

                            if (Array.isArray(parsedJson[libName].css)) {
								parsedJson[libName].css = parsedJson[libName].css.map(value => `/${libPath}/${value}`);
                            } else {
                                parsedJson[libName].css = `/${libPath}/${parsedJson[libName].css}`;
                            }

                        }

                        if (parsedJson[libName].js && parsedJson[libName].js.length) {
                            if (Array.isArray(parsedJson[libName].js)) {
                                parsedJson[libName].js = parsedJson[libName].js[0];
                            }

                            if(!parsedJson[libName].external) {
								parsedJson[libName].js = `/${libPath}/${parsedJson[libName].js}`;
                            }else{
                                delete parsedJson[libName].external;
                            }
                        }
                    }
                }

                return parsedJson;
            }
        }),


        gulp.dest(dirs.project.frontend.self)
    ).on("error", notify.onError());

});

gulp.task("dependencies", gulp.series('dependencies:local', 'dependencies:vendor'));


gulp.task('build', gulp.series('clean', 'copy', 'styles', 'js', 'dependencies'));


gulp.task('serve', function () {
    browserSync.init({
        proxy: domain,
        port: 8080,
        reloadDelay: 300,
        open: false
    });

    browserSync.watch([
        dirs.project.self,
        `${dirs.project.self}/**/*.*`,
        `!${dirs.project.self}/bitrix/cache/**/*.php`,
        `!${dirs.project.self}/bitrix/managed_cache/**/*.php`,
        `!${dirs.project.self}/bitrix/stack_cache/**/*.php`
    ]).on('change', browserSync.reload);
});

gulp.task('watch', function () {
    gulp.watch(filePaths.styles, {dot: true}, gulp.series("styles"));
    gulp.watch(filePaths.js, {dot: true}, gulp.series("js"));
    gulp.watch(filePaths.copy, {dot: true}, gulp.series("copy"));
});

gulp.task("dev", gulp.series('build', gulp.parallel("watch", "serve")));