<?
$aMenuLinks = Array(
	Array(
		"1С:Учебный центр", 
		"/services/studying_center/", 
		Array(), 
		Array("ICON"=>"icon_user"), 
		"" 
	),
	Array(
		"Центр сопровождение 1С, ИТС - Информационно технологическое сопровождение", 
		"/services/support_center/", 
		Array(), 
		Array("ICON"=>"icon_sign"), 
		"" 
	),
	Array(
		"Электронная отчетность", 
		"/services/remote_reports/", 
		Array(), 
		Array("ICON"=>"icon_checklist"), 
		"" 
	),
	Array(
		"Консультации по выбору ПО", 
		"/services/consultation/", 
		Array(), 
		Array("ICON"=>"icon_text-bubble-double"), 
		"" 
	),
	Array(
		"Доставка и установка ПО", 
		"/services/shipping-and-installing/", 
		Array(), 
		Array("ICON"=>"icon_truck-w-arrow"), 
		"" 
	),
	Array(
		"Настройка и внедрение ПО", 
		"/services/setting-and-inserting/", 
		Array(), 
		Array("ICON"=>"icon_gears"), 
		"" 
	)
);
?>