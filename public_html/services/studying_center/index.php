<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("not_show_wrapper_class", "N");
$APPLICATION->SetTitle("Учебный центр");

    ZLabs\JSCore::addBlocks(['sidebar', 'studyingCenter']);

?>

    <div class="clearfix">
        <div class="sidebar">
            <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"sidebar", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "180000",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "servicies",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "sidebar"
	),
	false
);?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_RECURSIVE" => "Y",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/local/included_areas/sidebar/block1.php"
                )
            );?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:main.include",
                "",
                Array(
                    "AREA_FILE_RECURSIVE" => "Y",
                    "AREA_FILE_SHOW" => "file",
                    "AREA_FILE_SUFFIX" => "inc",
                    "EDIT_TEMPLATE" => "",
                    "PATH" => "/local/included_areas/sidebar/block2.php"
                )
            );?>
        </div>
        <div class="content content_w-sidebar">
            <div class="studying-center text">
                <p>Основная цель Учебного Центра – предоставление пользователям программ «1С» возможности повысить уровень знаний и эффективность своей работы. Основываясь на своем значительном опыте работы, мы можем гарантировать Вам самый высокий уровень обучения. Наш центр предлагает курсы и семинары, ориентированные на любой уровень пользователей программ «1С».</p>
                <h2 class="studying-center__subheader">Мы предлагаем</h2>
                <ul class="text__list studying-center__list">
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Курсы, проводимые по сертифицированной фирмой «1С» методике" class="link text-list__title studying-center__title">Курсы, проводимые по сертифицированной фирмой «1С» методике</a>
                        <div class="text-list__desc studying-center__desc">Курсы предназначены для пользователей программ «1С: Предприятие 8»</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Сертифицированные курсы фирмы «1С» для школьников" class="link text-list__title studying-center__title">Сертифицированные курсы фирмы «1С» для школьников </a>
                        <div class="text-list__desc studying-center__desc">Курсы предназначены для школьников в возрасте 13-17 лет</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Консультационные семинары" class="link text-list__title studying-center__title">Консультационные семинары</a>
                        <div class="text-list__desc studying-center__desc">Для специалистов желающих углубленно освоить все аспекты автоматизированного учетас применением  продуктов фирмы «1С»</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Эксклюзивные мастер-классы" class="link text-list__title studying-center__title">Эксклюзивные мастер-классы</a>
                        <div class="text-list__desc studying-center__desc">Уникальные тренинги, как дополнение к имеющейся номенклатуре профессиональных учебных курсов и консультационных семинаров</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Авторские курсы" class="link text-list__title studying-center__title">Авторские курсы</a>
                    </li>
                </ul>
                <div class="studying-center__btn-block">
                    <div class="btn btn_linear studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Задать вопрос">Задать вопрос</a>
                    </div>
                    <div class="btn btn_main studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Подобрать курс">Подобрать курс</a>
                    </div>
                </div>
                <h2 class="studying-center__subheader">Формы обучения</h2>
                <table class="text__table studying-center__table">
                    <tbody>
                    <tr>
                        <th>Групповая</th>
                        <th>Корпоративная</th>
                        <th>Индивидуальная</th>
                    </tr>
                    <tr>
                        <td>Группы от 6 до 10 человек</td>
                        <td>Группы от 5 человек</td>
                        <td>Персональная программа обучения</td>
                    </tr>
                    <tr>
                        <td>Практические занятия на основе теории</td>
                        <td>Выбор места проведения занятий</td>
                        <td>Выбор места проведения занятий</td>
                    </tr>
                    <tr>
                        <td>Ответы на вопросы по тематике обучения</td>
                        <td>Обучение без отрыва от производства</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text__block studying-center__block">
                    <div class="text-block__header">Оснащение учебного класса</div>
                    <div class="text-block-wrap clearfix">
                        <div class="text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Современные компьютеры</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Безопасные ж/к мониторы</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Профессиональные проекторы</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Шумоизоляция учебных классов</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Система кондиционирования воздуха</span>
                            </div>
                        </div>
                        <div class="text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Вентиляция и очистка воздуха</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Антибактериальная очистка воздуха</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Кулер (очищенная питьевая вода)</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Инвентарь для кофе паузы</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Парковка вблизи от входа</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="studying-center__btn-block">
                    <div class="btn btn_linear studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Задать вопрос">Задать вопрос</a>
                    </div>
                    <div class="btn btn_main studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Подобрать курс">Подобрать курс</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>