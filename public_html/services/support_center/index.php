<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Центр сопровождение 1С, ИТС - Информационно технологическое сопровождение");

ZLabs\JSCore::addBlocks(['its', 'rate', 'review', 'similar', 'consul']);

?>
        <div class="container container_narrow">
            <div class="content">
                <div class="its text">
                    <p>Многолетний опыт работы нашей компании позволил создать уникальную систему сервиса и сопровождения пользователей программ 1С:Предприятие.</p>
                    <p>Уникальность технологии сопровождения заключается в наличии разработанного стандарта качества, который прошел сертификацию на соответствие всем требованиям фирмы "1С", что подтверждается высоким статусом Центра сопровождения фирмы "1С" и соответствующим сертификатом. Кроме того, стандарт качества сопровождения нашей компании прошел сертификацию на соответствие уровня международного стандарта качества, что так же подтверждено соответствующими международными сертификатами. Но самым главным подтверждением нашей квалификации является доверие наших клиентов и отзывы, благодаря которым мы постоянно совершенствуем свою работу. Ежемесячный рост клиентов компании подтверждает высокую востребованность созданной нами технологии. За время работы более 10000 компаний региона оценили все преимущества работы с нами.</p>
                </div>
                <div class="rate rate-ref">
                    <h2 class="rate__header">Тарифы сопровождения</h2>
                    <div class="rate__panels clearfix visibility">
                        <div class="rate__panel">
                            <div class="rate-panel__subheader">Тариф</div>
                            <div class="rate-panel__header">«Базовый»</div>
                            <div class="rate-panel__duration">
                                <span class="rate-panel-duration__text">При оплате за </span>
                                <button class="rate-panel__variant icon icon_arrow-angle-down">
                                <span class="rate-panel-variant__text">
                                    <span class="rate-panel-variant__current">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">12 месяцев</span>
                                            <span class="pop-up-list__item">24 месяца</span>
                                        </span>
                                    </span>
                                </span></button>
                            </div>
                            <div class="rate-panel__prices">
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Льготная цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">12 156 <span class="rate-panel-price__currency">"</span></div>
                                </div>
                                <hr>
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Возобновляемая цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">16 264 <span class="rate-panel-price__currency">"</span></div>
                                </div>
                            </div>
                            <div class="btn btn_linear rate-panel__btn">
                                <a href="#" class="link btn btn__link rate-panel__btn-link" title="Оставить заявку">Оставить заявку</a>
                            </div>
                            <a href="#rate-tabs__tabpanel1" class="link rate-panel-about__link rate-ref__link" title="Подробности" role="tab" data-toggle="tab">Подробности</a>
                        </div>
                        <div class="rate__panel">
                            <div class="rate-panel__subheader">Тариф</div>
                            <div class="rate-panel__header">«Оптимальный»</div>
                            <div class="rate-panel__duration">
                                <span class="rate-panel-duration__text">При оплате за </span>
                                <button class="rate-panel__variant icon icon_arrow-angle-down">
                                <span class="rate-panel-variant__text">
                                    <span class="rate-panel-variant__current ">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">12 месяцев</span>
                                            <span class="pop-up-list__item">24 месяца</span>
                                        </span>
                                    </span>
                                </span>
                                </button>
                            </div>
                            <div class="rate-panel__prices">
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Льготная цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">29 664 <span class="rate-panel-price__currency">"</span></div>
                                </div>
                                <hr>
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Возобновляемая цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">35 592 <span class="rate-panel-price__currency">"</span></div>
                                </div>
                            </div>
                            <div class="btn btn_linear rate-panel__btn">
                                <a href="#" class="link btn btn__link rate-panel__btn-link" title="Оставить заявку">Оставить заявку</a>
                            </div>
                            <a href="#rate-tabs__tabpanel2" class="link rate-panel-about__link rate-ref__link" title="Подробности" role="tab" data-toggle="tab">Подробности</a>
                        </div>
                        <div class="rate__panel">
                            <div class="rate-panel__subheader">Тариф</div>
                            <div class="rate-panel__header">«Расширенный»</div>
                            <div class="rate-panel__duration">
                                <span class="rate-panel-duration__text">При оплате за </span>
                                <button class="rate-panel__variant icon icon_arrow-angle-down">
                                <span class="rate-panel-variant__text">
                                    <span class="rate-panel-variant__current">24 месяца</span>
                                    <span class="pop-up pop-up-list-wrap pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item">12 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">24 месяца</span>
                                        </span>
                                    </span>
                                </span></button>
                            </div>
                            <div class="rate-panel__prices">
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Льготная цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">42 659 <span class="rate-panel-price__currency">"</span></div>
                                </div>
                            </div>
                            <div class="btn btn_linear rate-panel__btn">
                                <a href="#" class="link btn btn__link rate-panel__btn-link" title="Оставить заявку">Оставить заявку</a>
                            </div>
                            <a href="#rate-tabs__tabpanel3" class="link rate-panel-about__link rate-ref__link" title="Подробности">Подробности</a>
                        </div>
                    </div>
                    <div class="rate-btn-wrap">
                        <button class="btn btn_trigger rate__btn rate-ref__hider">Скрыть подробности</button>
                        <div class="btn btn_main rate__btn"><a href="#" class="link btn__link rate__btn-link" role="tab" data-toggle="tab">Подобрать тариф</a></div>
                    </div>
                    <div class="rate-tabs rate-ref__anchor">
                        <div class="rate-tabs__tablist" role="tablist">
                            <div class="rate-tabs-tab-wrap">
                                <a href="#rate-tabs__tabpanel1" class="link rate-tabs__tab rate-tabs__tab_active" title="Базовый" role="tab" data-toggle="tab">Базовый</a>
                            </div>
                            <div class="rate-tabs-tab-wrap">
                                <a href="#rate-tabs__tabpanel2" class="link rate-tabs__tab" title="Оптимальный" role="tab" data-toggle="tab">Оптимальный</a>
                            </div>
                            <div class="rate-tabs-tab-wrap">
                                <a href="#rate-tabs__tabpanel3" class="link rate-tabs__tab" title="Расширенный" role="tab" data-toggle="tab">Расширенный</a>
                            </div>
                        </div>
                        <div class="rate-tabs__tabpanels">
                            <div class="rate-tabs__tabpanel tab-panel tab-panel_active tab-panel_in" role="tabpanel" id="rate-tabs__tabpanel1">
                                <table class="rate-tab__table">
                                    <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Условия предоставления</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического обновления</td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством удаленного подключенияили с выездом специалиста</td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка комплекта поставки (DVD-выпуск, текущий выпускжурнала "БУХ.1С", сувенир делового назначения)</td>
                                        <td>Сервис-инженер или экспедитор или почта России</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Линия консультаций фирмы «1С»</td>
                                        <td>с 10-00 до 18-00 по рабочим дням, e-mail:v8@1c.ru, по телефону (495) 956-11-81</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического обновления</td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством удаленного подключенияили с выездом специалиста</td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка комплекта поставки (DVD-выпуск, текущий выпускжурнала "БУХ.1С", сувенир делового назначения)</td>
                                        <td>Сервис-инженер или экспедитор или почта России</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rate-tabs__tabpanel tab-panel" role="tabpanel" id="rate-tabs__tabpanel2">
                                <table class="rate-tab__table">
                                    <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Условия предоставления</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического обновления</td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством удаленного подключенияили с выездом специалиста</td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка комплекта поставки (DVD-выпуск, текущий выпускжурнала "БУХ.1С", сувенир делового назначения)</td>
                                        <td>Сервис-инженер или экспедитор или почта России</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Линия консультаций фирмы «1С»</td>
                                        <td>с 10-00 до 18-00 по рабочим дням, e-mail:v8@1c.ru, по телефону (495) 956-11-81</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического обновления</td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rate-tabs__tabpanel tab-panel" role="tabpanel" id="rate-tabs__tabpanel3">
                                <table class="rate-tab__table">
                                    <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Условия предоставления</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического обновления</td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством удаленного подключенияили с выездом специалиста</td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="review student-review student-review_short visibility">
            <div class="container container_narrow">
                <div class="review__head student-review__head">
                    <h2 class="h2 review__header review__header_center">Отзывы наших клиентов</h2>
                </div>
                <div class="review__body">
                    <div class="review__slider">
                        <div class="review-slide-wrap">
                            <div class="review__slide">
                                <div class="review-slide__img"></div>
                                <div class="review-slide__title-and-text">
                                    <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                    <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по курсам, разработанным фирмой «1С».</div>
                                </div>
                            </div>
                        </div>
                        <div class="review-slide-wrap">
                            <div class="review__slide">
                                <div class="review-slide__img"></div>
                                <div class="review-slide__title-and-text">
                                    <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                    <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по курсам, разработанным фирмой «1С».</div>
                                </div>
                            </div>
                        </div>
                        <div class="review-slide-wrap">
                            <div class="review__slide">
                                <div class="review-slide__img"></div>
                                <div class="review-slide__title-and-text">
                                    <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                    <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по курсам, разработанным фирмой «1С».</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="review__footer">
                    <div class="btn btn_linear review__btn student-review__btn"><a href="#" class="link btn__link review__btn-link student-review__btn-link" title="Читать отзыв целиком">Читать отзыв целиком</a></div>
                    <div class="btn btn_main review__btn review__btn_main"><a href="#review__form" class="link btn__link review__btn-link review__pop-up-opener" title="Оставить свой отзыв">Оставить свой отзыв</a></div>
                </div>
            </div>
            <div class="review__form feedback-form-modal-wrap" id="review__form">
                <div class="feedback-form__head">
                    <h1 class="feedback-form__header">Добавить отзыв</h1>
                </div>
                <form action="#" class="feedback-form feedback-fom_modal">
                    <div class="feedback-form-group">
                        <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Введите ФИО">
                    </div>
                    <div class="feedback-form-group">
                        <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_tall" placeholder="Текст отзыва"></textarea>
                    </div>
                    <div class="feedback-form__footer">
                        <div class="feedback-form-group clearfix">
                            <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                                <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                                <span class="custom-control__indicator icon icon_checked"></span>
                                <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                            </label>
                        </div>
                        <div class="feedback-form-group">
                            <button class="btn btn_main feedback-form__btn"><span class="btn__link">Добавить отзыв</span></button>
                            <div class="feedback-form__footnote"></div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <section class="similar similar_shorter visibility">
            <div class="similar__head">
                <div class="container container_narrow">
                    <h2 class="similar__header">Вы можете посетить другие наши курсы</h2>
                    <div class="similar-slider__toolbar clearfix">
                    </div>
                </div>
            </div>
            <div class="similar__body">
                <div class="similar__slider">
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                            <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='Основы программированияв «1С: Предприятие 8» для школьников'>
                            <span class="similar-slide__title">Основы программирования в «1С: Предприятие 8» для школьников</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                            <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Управление торговлей". Практическое применение типовой конфигурации'>
                            <span class="similar-slide__title">1С:Предприятие 8. Управление торговлей". Практическое применение типовой конфигурации</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                            <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='"Ведение бюджетного учета в программе "1С:Бухгалтерия государственного учреждения'>
                            <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                    <div class="similar-slide-wrap">
                        <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                            <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                            <span class="similar-slide__value">Стоимость курса</span>
                            <span class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></span>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="consul">
            <div class="container container_narrow">
                <h1 class="consul__header">Необходима консультация?</h1>
                <div class="consul__desc">Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов нашей фирмы</div>
                <form action="#" class="consul__form feedback-form clearfix">
                    <div class="feedback-form-group clearfix">
                        <div class="consul__item">
                            <div class="feedback-form-group">
                                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
                            </div>
                        </div>
                        <div class="consul__item">
                            <div class="feedback-form-group">
                                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
                            </div>
                        </div>
                        <div class="consul__item">
                            <div class="feedback-form-group">
                                <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                                <ul class="feedback-form__control_type_options">
                                    <li class="feedback-form__control_type_option">Город 1</li>
                                    <li class="feedback-form__control_type_option">Город 2</li>
                                    <li class="feedback-form__control_type_option">Город 3</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="feedback-form-group">
                        <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
                    </div>
                    <div class="feedback-form-group">
                        <button class="btn btn_main feedback-form__btn"><span class="btn__link">Получить консультацию</span></button>
                        <div class="feedback-form__footnote"></div>
                    </div>
                    <div class="feedback-form-group clearfix">
                        <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                            <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                            <span class="custom-control__indicator icon icon_checked"></span>
                            <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                        </label>
                    </div>
                </form>
            </div>
        </section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>