<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Наши услуги");
ZLabs\JSCore::addBlocks(['services']);
?>

    <div class="content container container_narrow">
        <div class="services text">
            <p>Наша компания оказывает полный перечень услуг по внедрению систем автоматизации на базе программных продуктов «1С», используя передовые технологии работы и накопленный опыт автоматизации компаний малого, среднего и крупного бизнеса.</p>
            <p>Наши специалисты помогут сформировать оптимальный для Вашего бизнеса и Ваших задач пакет услуг по автоматизации - учитывающий не только сиюминутные требования, но и перспективы развития Вашего бизнеса. </p>
            <div class="text__block">
                <div class="text-block__header">Предоставляемый спектр услуг</div>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "serviciesMenu",
                    Array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(0=>"",),
                        "MENU_CACHE_TIME" => "180000",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "servicies",
                        "USE_EXT" => "N"
                    )
                );?>
            </div>
            <p>Опыт внедрений программ семейства «1С:Предприятие 8» показывает, что оптимальное решение бизнес-задач предприятия требует комплексного подхода: от определения целей компании до оптимизации процессов управления и построения системы информационного обеспечения с учетом специфики бизнеса.</p>
        </div>
    </div>
<?$APPLICATION->IncludeComponent(
    "zlabs:feedbackform.form",
    "consul.form",
    Array(
        "COMPONENT_TEMPLATE" => "consul.form",
        "EMAIL_TO" => array(0=>"info@mskmv.ru",1=>"",),
        "EVENT_MESSAGE_ID" => array(0=>"8",),
        "FIELD_0_CODE" => "NAME",
        "FIELD_0_MASK" => "SIMPLE",
        "FIELD_0_NOTE" => "",
        "FIELD_0_PLACEHOLDER" => "Ваше имя",
        "FIELD_0_REQUIRE" => "Y",
        "FIELD_0_TITLE" => "Имя отправителя",
        "FIELD_0_TYPE" => "TEXT",
        "FIELD_1_CODE" => "TEL",
        "FIELD_1_MASK" => "SIMPLE",
        "FIELD_1_NOTE" => "",
        "FIELD_1_PLACEHOLDER" => "Ваш телефон",
        "FIELD_1_REQUIRE" => "Y",
        "FIELD_1_TITLE" => "Телефон отправителя",
        "FIELD_1_TYPE" => "TEXT",
        "FIELD_2_CODE" => "CITIES",
        "FIELD_2_NOTE" => "",
        "FIELD_2_PLACEHOLDER" => "Выберите город",
        "FIELD_2_POSSIBLE_VALUES" => $GLOBALS["CITIES"],
        "FIELD_2_REQUIRE" => "N",
        "FIELD_2_TITLE" => "Город",
        "FIELD_2_TYPE" => "LIST",
        "FIELD_3_CODE" => "TEXT",
        "FIELD_3_HEIGHT" => "SHORT",
        "FIELD_3_NOTE" => "",
        "FIELD_3_PLACEHOLDER" => "Ваш вопрос",
        "FIELD_3_REQUIRE" => "Y",
        "FIELD_3_TITLE" => "Вопрос",
        "FIELD_3_TYPE" => "TEXTAREA",
        "FOOTNOTE" => "",
        "GOALS" => array(0=>"DEALER_MAIN_PAGE",1=>"",),
        "ID" => "",
        "LINK_TO_FORM" => "Тестовая ссылкана модальное окно",
        "NAME" => "Консультация",
        "NUM_FIELDS" => "4",
        "POPUP_FORM" => "N",
        "SUBMIT" => "Получить консультацию",
        "SUB_TITLE" => "Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов нашей фирмы",
        "SUCCESS_MESSAGE" => "В близжайшее время с вами свяжется наш менеджер",
        "SUCCESS_MESSAGE_TITLE" => "Ваша заявка принята",
        "TITLE" => "Необходима консультация?",
        "USER_CONSENT" => "Y",
        "USER_CONSENT_ID" => "1",
        "USER_CONSENT_IS_CHECKED" => "Y",
        "USER_CONSENT_IS_LOADED" => "N"
    )
);?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>