<?php
/**
 * todo: как временное решение обрабатывать вручную vendor.json и custom.json
 */

namespace ZLabs;


class JSCore
{
    const FRONTEND_DIRECTORY = '/local/frontend/';

    protected static $arBlocks = array();
    protected static $jsonBlocksFilePath = '/local/frontend/frontend.json';
    protected static $jsonTempVendorBlocksFilePath = '/local/frontend/vendor/vendor.json';

    public static function addBlocks(array $arBlocks)
    {
        static::$arBlocks = array_merge(self::$arBlocks, $arBlocks);
    }

    public static function onEpilogHandler()
    {
        static::registerBlocksFromJson();
        static::initBlocks();
    }


    public static function registerBlocksFromJson()
    {
        //static::tempRegisterVendorBlocks();
        $jsonFile = file_get_contents($_SERVER['DOCUMENT_ROOT'] . static::$jsonBlocksFilePath);
        $arBlocks = json_decode($jsonFile, true);
        if (is_array($arBlocks)) {
            static::registerBlocksFromArray($arBlocks);
        }
    }

    public static function registerBlocksFromArray($arBlocks, $dir = '')
    {
        foreach ($arBlocks as $blockName => $arBlock) {
            if (!isset($arBlock['skip_core'])) {
                $arBlock['skip_core'] = true;
            }
            if (!preg_match('~[a-z0-9_]+~', $blockName)) {
                throw new \Exception('Попытка зарегистрировать библиотеку с некорректным именем - "' . $blockName . '"');
            }

            \CJSCore::RegisterExt($blockName, static::normalizeBlockPath($arBlock, $dir));
        }
    }

    public static function tempRegisterVendorBlocks()
    {
        $jsonFile = file_get_contents($_SERVER['DOCUMENT_ROOT'] . static::$jsonTempVendorBlocksFilePath);
        $arBlocks = json_decode($jsonFile, true);
        if (is_array($arBlocks)) {
            static::registerBlocksFromArray($arBlocks, static::FRONTEND_DIRECTORY . 'vendor/');
        }
    }

    protected static function normalizeBlockPath($arBlock, $dir = '')
    {
        if ($dir !== '') {
            $arBlock['js'] = $dir . $arBlock['js'];
            if (isset($arBlock['css'])) {
                if (!is_array($arBlock['css'])) {
                    $arBlock['css'] = $dir . $arBlock['css'];
                } else {
                    foreach ($arBlock['css'] as &$path) {
                        $path = $dir . $path;
                    }
                    unset($path);
                }
            }
        }
        return $arBlock;
    }

    public static function initBlocks()
    {
        return \CJSCore::Init(static::$arBlocks);
    }
}
