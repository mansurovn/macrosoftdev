<?php

namespace ZLabs;

use Bitrix\Main\Context;

class RestartBuffer
{
    public static function restartBufferIsAjax()
    {
        if (self::isAjax()) {
            $GLOBALS['APPLICATION']->RestartBuffer();
        }
    }

    public static function isAjax()
    {
        $request = Context::getCurrent()->getRequest();
        return !empty($request->get('is_ajax')) && $request->get('is_ajax') == 'y';
    }
}
