<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
        </main>
    <footer class="footer">
        <?$APPLICATION->IncludeComponent(
	"zlabs:feedbackform.form", 
	"feedback.form", 
	array(
		"COMPONENT_TEMPLATE" => "feedback.form",
		"EMAIL_TO" => array(
			0 => "info@mskmv.ru",
			1 => "",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "8",
		),
		"FIELD_0_CODE" => "NAME",
		"FIELD_0_MASK" => "SIMPLE",
		"FIELD_0_NOTE" => "",
		"FIELD_0_PLACEHOLDER" => "Ваше имя",
		"FIELD_0_REQUIRE" => "Y",
		"FIELD_0_TITLE" => "Имя отправителя",
		"FIELD_0_TYPE" => "TEXT",
		"FIELD_1_CODE" => "TEL",
		"FIELD_1_MASK" => "PHONE",
		"FIELD_1_NOTE" => "",
		"FIELD_1_PLACEHOLDER" => "Ваш телефон",
		"FIELD_1_REQUIRE" => "Y",
		"FIELD_1_TITLE" => "Телефон отправителя",
		"FIELD_1_TYPE" => "TEXT",
		"FOOTNOTE" => "",
		"GOALS" => array(
			0 => "DEALER_MAIN_PAGE",
			1 => "",
		),
		"ID" => "consul-form",
		"LINK_TO_FORM" => "",
		"NAME" => "Форма для получение консультации",
		"NUM_FIELDS" => "4",
		"POPUP_FORM" => "Y",
		"SUBMIT" => "Получить консультацию",
		"SUB_TITLE" => "Бесплатно проконсультироваться, уточнить цены и
<br>
заказать решение можно у специалистов нашей фирмы",
		"SUCCESS_MESSAGE" => "В близжайшее время с вами свяжется наш менеджер",
		"SUCCESS_MESSAGE_TITLE" => "Ваша заявка принята",
		"TITLE" => "Нужна консультация?",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_ID" => "1",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"FIELD_2_TYPE" => "LIST",
		"FIELD_2_TITLE" => "Список городов",
		"FIELD_2_CODE" => "CITIES",
		"FIELD_2_PLACEHOLDER" => "Выберите город",
		"FIELD_2_POSSIBLE_VALUES" => $GLOBALS["CITIES"],
		"FIELD_2_REQUIRE" => "N",
		"FIELD_2_NOTE" => "",
		"FIELD_3_TYPE" => "TEXTAREA",
		"FIELD_3_TITLE" => "Вопрос",
		"FIELD_3_CODE" => "TEXT",
		"FIELD_3_PLACEHOLDER" => "Ваш вопрос",
		"FIELD_3_REQUIRE" => "Y",
		"FIELD_3_NOTE" => "",
		"FIELD_3_HEIGHT" => "SHORT"
	),
	false
);?>
        <?$APPLICATION->IncludeComponent(
	"zlabs:feedbackform.form", 
	"feedback.form", 
	array(
		"COMPONENT_TEMPLATE" => "feedback.form",
		"EMAIL_TO" => array(
			0 => "info@mskmv.ru",
			1 => "",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "8",
		),
		"FIELD_0_CODE" => "NAME",
		"FIELD_0_MASK" => "SIMPLE",
		"FIELD_0_NOTE" => "",
		"FIELD_0_PLACEHOLDER" => "Ваше имя",
		"FIELD_0_REQUIRE" => "Y",
		"FIELD_0_TITLE" => "Имя отравителя",
		"FIELD_0_TYPE" => "TEXT",
		"FIELD_1_CODE" => "EMAIL",
		"FIELD_1_MASK" => "EMAIL",
		"FIELD_1_NOTE" => "",
		"FIELD_1_PLACEHOLDER" => "Ваш email",
		"FIELD_1_REQUIRE" => "Y",
		"FIELD_1_TITLE" => "Почта отправителя",
		"FIELD_1_TYPE" => "TEXT",
		"FOOTNOTE" => "",
		"GOALS" => array(
			0 => "DEALER_MAIN_PAGE",
			1 => "",
		),
		"ID" => "feeedback-form",
		"LINK_TO_FORM" => "",
		"NAME" => "Форма для обратной связи",
		"NUM_FIELDS" => "3",
		"POPUP_FORM" => "Y",
		"SUBMIT" => "Отправить сообщение",
		"SUB_TITLE" => "",
		"SUCCESS_MESSAGE" => "В близжайшее время с вами свяжется наш менеджер",
		"SUCCESS_MESSAGE_TITLE" => "Ваша заявка принята",
		"TITLE" => "Написать нам",
		"USER_CONSENT" => "Y",
		"USER_CONSENT_ID" => "1",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"FIELD_2_TYPE" => "TEXTAREA",
		"FIELD_2_TITLE" => "Вопрос",
		"FIELD_2_CODE" => "TEXT",
		"FIELD_2_PLACEHOLDER" => "Ваш вопрос",
		"FIELD_2_REQUIRE" => "Y",
		"FIELD_2_NOTE" => "",
		"FIELD_2_HEIGHT" => "SHORT"
	),
	false
);?>
        <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
            <div class="feedback-form__head">
                <h1 class="feedback-form__header">Нужна консультация?</h1>
                <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у специалистов нашей фирмы</div>
            </div>

            <form action="#" class="feedback-form feedback-form_modal">
                <div class="feedback-form-group">
                    <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
                </div>
                <div class="feedback-form-group">
                    <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
                </div>
                <div class="feedback-form-group">
                    <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                    <ul class="feedback-form__control_type_options">
                        <li class="feedback-form__control_type_option">Город 1</li>
                        <li class="feedback-form__control_type_option">Город 2</li>
                        <li class="feedback-form__control_type_option">Город 3</li>
                    </ul>
                </div>
                <div class="feedback-form-group">
                    <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
                </div>
                <div class="form__footer">
                    <div class="feedback-form-group clearfix">
                        <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                            <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                            <span class="custom-control__indicator icon icon_checked"></span>
                            <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                        </label>
                    </div>
                    <div class="feedback-form-group">
                        <button class="btn btn_main feedback-form__btn"><span class="btn__link">Получить консультацию</span></button>
                        <div class="feedback-form__footnote"></div>
                    </div>
                </div>
            </form>

        </div>
        <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
            <div class="feedback-form__head">
                <h1 class="feedback-form__header">Написать нам</h1>
            </div>
            <form action="#" class="feedback-form feedback-form_modal">
                <div class="feedback-form-group">
                    <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
                </div>
                <div class="feedback-form-group">
                    <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
                </div>
                <div class="feedback-form-group">
                    <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
                </div>
                <div class="form__footer">
                    <div class="feedback-form-group clearfix">
                        <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                            <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                            <span class="custom-control__indicator icon icon_checked"></span>
                            <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                        </label>
                    </div>
                    <div class="feedback-form-group">
                        <button class="btn btn_main feedback-form__btn"><span class="btn__link">Отправить сообщение</span></button>
                        <div class="feedback-form__footnote"></div>
                    </div>

                </div>
            </form>
        </div>
        <div class="footer__menu clearfix">
            <div class="container container_narrow">
                <div class="footer-menu__item">
                    <div class="footer-menu__header">1С: Предприятие 8</div>
                    <ul class="footer-menu__list">
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление торговлей">1С: Управление торговлей</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и Управление персоналом">1С: Зарплата и Управление персоналом</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Комплексная автоматизация">1С: Комплексная автоматизация</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление производственным предприятием">1С: Управление<br>производственным предприятием</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление небольшой фирмой">1С: Управление небольшой<br>фирмой</a></li>
                    </ul>
                </div>
                <div class="footer-menu__item">
                    <div class="footer-menu__header">Для бюджетников</div>
                    <ul class="footer-menu__list">
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия государственного учреждения">1С: Бухгалтерия<br>государственного учреждения</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и кадры бюджетного учреждения">1С: Зарплата и кадры<br>бюджетного учреждения</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Государственные и муниципальные закупки">1С: Государственные и муниципальные закупки</a></li>
                    </ul>
                </div>
                <div class="footer-menu__item footer-menu__item_narrow">
                    <div class="footer-menu__header">Услуги</div>
                    <ul class="footer-menu__list">
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Доставка и установка">Доставка и установка</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Настройка и внедрение">Настройка и внедрение</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Учебный центр">1С: Учебный центр</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Электронная отчетность из 1С: Предприятия 8">Электронная отчетность из<br>1С: Предприятия 8</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                    </ul>
                </div>
                <div class="footer-menu__item">
                    <div class="footer-menu__header">О компании</div>
                    <ul class="footer-menu__list">
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Статусы компании">Статусы компании</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Наши сотрудники">Наши сотрудники</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a></li>
                        <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer__contact">
            <div class="container container_narrow">
                <div class="footer-contact__item">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/included_areas/footer/address1.php"
                        )
                    );?>
                </div>
                <div class="footer-contact__item">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/included_areas/footer/address2.php"
                        )
                    );?>
                </div>
                <div class="footer-contact__item">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        ".default",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "COMPONENT_TEMPLATE" => ".default",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/local/included_areas/footer/address3.php"
                        )
                    );?>
                </div>
                <div class="footer-contact__item">
                    <div class="footer-contact-btn-wrap">
                        <div class="btn btn_main footer-contact__btn"><a href="#consul-form" class="link btn__link footer-contact__btn-link footer__consul-opener" title="Получить консультацию">Получить консультацию</a></div>
                        <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form" class="link btn__link footer-contact__btn-link footer__feedback-opener" title="Написать нам">Написать нам</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer__signs">
            <div class="container container_narrow">
                <div class="footer__copyright">© 2002–2017. Все права защищены. </div>
                <div class="footer__dev-sign">
                    <div class="footer-dev-sign__title">Создание сайта</div>
                    <div class="footer-dev-sign__name">Студия Z-labs</div>
                </div>
            </div>
        </div>
    </footer>
    </body>
</html>