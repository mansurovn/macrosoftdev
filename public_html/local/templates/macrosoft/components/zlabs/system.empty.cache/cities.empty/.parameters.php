<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arTemplateParameters = array(
    'IBLOCK_ID' => Array(
        'NAME' => GetMessage('Z_SEC_IBLOCK_ID'),
        'TYPE' => 'STRING',
    ),
    'GLOBAL_VAR' => Array(
        'NAME' => GetMessage('Z_SEC_GLOBAL_VAR'),
        'TYPE' => 'STRING',
    )
);