<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */

if (!CModule::includeModule('iblock')) {
    return;
}

$arCities = array();
$rsCity = CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'),
    false,
    false,
    array('NAME')
);
while ($arCity = $rsCity->GetNext()) {
    $arCities[] = $arCity['NAME'];
}

$component = $this->__component;
if (is_object($component)) {
    $component->arResult['CITIES'] = $arCities;
    $component->setResultCacheKeys(array('CITIES'));
}