<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $templateData */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var string $epilogFile */
/** @var CBitrixComponent $component */

if ($arResult['CITIES']) {
    $GLOBAL_VAR = (string)$arParams['GLOBAL_VAR'];
    global ${$GLOBAL_VAR};
    if (!is_array(${$GLOBAL_VAR})) {
        ${$GLOBAL_VAR} = $arResult['CITIES'];
    } else {
        ${$GLOBAL_VAR} = array_merge(${$GLOBAL_VAR}, $arResult['CITIES']);
    }
}