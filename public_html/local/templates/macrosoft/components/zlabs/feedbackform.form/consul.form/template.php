<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>

<!--noindex-->
<script id="feedback-success-message" type="text/html">
    <?php include($_SERVER['DOCUMENT_ROOT'] . '/' . $this->GetFolder() . '/feedback-success-message.mustache'); ?>
</script>
<div>
    <div class="consul">
        <div class="container container_narrow">
        <? if ($arResult['TITLE']): ?>
            <div class="h1 consul__header"><?= $arResult['TITLE'] ?></div>
            <? if ($arResult['SUB_TITLE']): ?>
                <div class="consul__desc"><?= $arResult['SUB_TITLE'] ?></div>
            <? endif ?>
        <? endif;?>
        <form class="consul__form feedback-form"
              enctype="multipart/form-data"
              method="post">
            <?$i=0;?>

            <?foreach ($arResult['FIELDS'] as $fieldKey => $field) :
                $textareaHeiht = '';
                switch ($field["HEIGHT"]) {
                    case "DEFAULT":
                        $textareaHeiht = '';
                        break;
                    case "TALL":
                        $textareaHeiht = 'feedback-form__control_type_textarea_tall';
                        break;
                    case "SHORT":
                        $textareaHeiht = 'feedback-form__control_type_textarea_short';
                        break;
                }

                $requiredCssClass = in_array($fieldKey, $arResult['REQUIRE_FIELDS']) ? ' feedback-form__control_required' : '';
                $placeholder = (in_array($fieldKey, $arResult['REQUIRE_FIELDS']) ? $field['PLACEHOLDER']
                    . '*' : $field['PLACEHOLDER']);
                ?>
                <?if ($i == 0) :?>
                    <div class="feedback-form__group clearfix">
                <?endif;?>
                    <?if ($i<3) :?>
                        <div class="consul__item">
                    <?endif;?>
                            <div class="feedback-form__group">
                                <? switch ($field['TYPE']) :
                                    case 'TEXT' :
                                        $validCssClass = '';
                                        switch ($field['MASK']) {
                                            case 'PHONE':
                                                $validCssClass = ' feedback-form__control_valid_phone';
                                                break;
                                            case 'EMAIL':
                                                $validCssClass = ' feedback-form__control_valid_email';
                                                break;
                                            case 'TEXT_RU':
                                                $validCssClass = ' feedback-form__control_valid_text-ru';
                                                break;
                                            case 'PHONE_OR_EMAIL':
                                                $validCssClass = ' feedback-form__control_valid_phone_or_email';
                                                break;
                                        }
                                        ?>
                                        <input class="feedback-form__control feedback-form__control_type_text<?= $requiredCssClass
                                        . $validCssClass ?>"
                                               type="text"
                                               name="<?= $field['CODE'] ?>"
                                               value="<?= $field['VALUE'] ?>"
                                               placeholder="<?= $placeholder ?>">
                                        <? break;
                                    case 'TEXTAREA': ?>
                                        <textarea
                                                class="feedback-form__control feedback-form__control_type_textarea <?=$textareaHeiht?> <?= $requiredCssClass ?>"
                                                name="<?= $field['CODE'] ?>"
                                                placeholder="<?= $placeholder ?>"><?= $field['VALUE'] ?></textarea>
                                        <? break;
                                    case 'LIST':
                                        if ($field['POSSIBLE_VALUES']) : ?>
                                            <select name="<?= $field['CODE'] ?>"
                                                    class="feedback-form__control feedback-form__control_type_list<?= $requiredCssClass ?>"
                                                    title="<?= $field['PLACEHOLDER'] ?>">
                                                <option value=""><?= $field['PLACEHOLDER'] ?></option>
                                                <? foreach ($field['POSSIBLE_VALUES'] as $fieldValue) : ?>
                                                    <option<?= $fieldValue == $field['VALUE']
                                                        ? ' selected' : '' ?>><?= $fieldValue ?></option>
                                                <? endforeach ?>
                                            </select>
                                            <?
                                        endif;
                                        break;
                                    case 'RADIO':
                                        if ($field['POSSIBLE_VALUES']) :
                                            foreach ($field['POSSIBLE_VALUES'] as $fieldValue): ?>
                                                <label class="feedback-form__label custom-control custom-control_type_radio"><input
                                                            class="custom-control__input feedback-form__control feedback-form__control_type_radio"
                                                            type="radio"
                                                            name="<?= $field['CODE'] ?>"
                                                            value="<?= $fieldValue ?>"<?= $fieldValue == $field['VALUE']
                                                        ? ' checked' : '' ?>>
                                                    <span class="custom-control__indicator"></span>
                                                    <span class="custom-control__name"><?= $fieldValue ?></span>
                                                </label>
                                            <? endforeach;
                                        endif;
                                        break;
                                    case 'CHECKBOX':
                                        if ($field['POSSIBLE_VALUES']) :
                                            foreach ($field['POSSIBLE_VALUES'] as $fieldValue): ?>
                                                <label class="feedback-form__label custom-control custom-control_type_checkbox"><input
                                                            class="custom-control__input feedback-form__control feedback-form__control_type_checkbox"
                                                            type="checkbox"
                                                            name="<?= $field['CODE'] ?>[]"
                                                            value="<?= $fieldValue ?>"<?= $fieldValue == $field['VALUE']
                                                        ? ' checked' : '' ?>>
                                                    <span class="custom-control__indicator icon icon_checked"></span>
                                                    <span class="custom-control__name"><?= $fieldValue ?></span>
                                                </label>
                                            <? endforeach;
                                        endif;
                                        break;
                                    case 'FILE':
                                        $multiple = $field['MULTIPLE'] == 'Y';
                                        ?>
                                        <input class="feedback-form__control feedback-form__control_type_file<?= $requiredCssClass ?>"
                                               type="file"
                                               name="<?= $field['CODE'] . ($multiple ? '[]' : '') ?>"<?= $multiple ? ' multiple' : '' ?>>
                                        <a class="feedback-form__pseudo-file-control"
                                           href="#"><span class="feedback-form__link"><?= $placeholder ?></span></a>
                                        <div class="feedback-form__files-list visibility visibility_manual"></div>
                                        <? break;
                                    case 'HIDDEN': ?>
                                        <input type="hidden" name="<?= $field['CODE'] ?>" value="<?= $field['VALUE'] ?>">
                                        <? break;
                                endswitch;
                                if ($field['NOTE']) : ?>
                                    <div class="feedback-form__note"><?= $field['NOTE'] ?></div>
                                <? endif ?>
                            </div>
                    <?if ($i<3) :?>
                        </div>
                    <?endif;?>
                <?if ($i == 2) :?>
                    </div>
                <?endif;?>
                <?php
                $i++;
            endforeach;?>
            <?
            $disabledSubmit = ($arParams['USER_CONSENT'] === 'Y' && $arParams['USER_CONSENT_IS_CHECKED'] === 'N')
                ? 'disabled="disabled"' : ''
            ?>
            <div class="feedback-form__submit-container">
                <button class="btn btn_main feedback-form__submit" type="submit"
                        name="submit"<?= $disabledSubmit ?>><span class="btn__link"><?= $arResult['SUBMIT'] ?></span></button>
                <div class="feedback-form__footnote"><?= $arResult['FOOTNOTE'] ?></div>
            </div>
            <?php
            if ($arParams['USER_CONSENT'] === 'Y') : ?>
                <div class="feedback-form__user-consent">
                    <?php
                    $APPLICATION->IncludeComponent('bitrix:main.userconsent.request', 'simple', Array(
                        'ID' => $arParams['USER_CONSENT_ID'],    // Соглашение
                        'IS_CHECKED' => $arParams['USER_CONSENT_IS_CHECKED'],    // Галка согласия проставлена по умолчанию
                        'AUTO_SAVE' => 'Y',    // Сохранять автоматически факт согласия
                        'IS_LOADED' => $arParams['USER_CONSENT_IS_LOADED'],    // Загружать текст соглашения сразу
                    ),
                        false
                    );
                    ?>
                </div>
                <?php
            endif; ?>
        </form>
    </div>
</div>
<?php
if (!empty($arResult['LINK_TO_FORM']) && $arResult['POPUP_FORM'] == 'Y') : ?>
    <a href="#<?= $arParams['AJAX_COMPONENT_ID'] ?>" class="link feedback-form-link"><?= $arResult['LINK_TO_FORM'] ?></a>
    <?php
endif; ?>
<script>
    $('#<?= $arParams['AJAX_COMPONENT_ID'] ?>').feedbackForm();
</script>
<!--/noindex-->