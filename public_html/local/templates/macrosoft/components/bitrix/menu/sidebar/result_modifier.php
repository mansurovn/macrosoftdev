<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult);

if ($needObtainMustache) {
    //$arResult = [];

    foreach ($arResult as $itemKey => $arItem) {

        $arMustacheItem = [
            'name' => $arItem['TEXT'],
            'link' => $arItem["LINK"],
            'linkActive' => $itemKey ===0 ? 'sidebar-menu__link_active' : ''
        ];
        $arResult['MUSTACHE']['items'][] = $arMustacheItem;
    }
}