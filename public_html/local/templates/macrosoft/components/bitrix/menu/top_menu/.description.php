<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateDescription = array(
	"NAME" => GetMessage("DEFAULT_NAV_NAME"),
	"DESCRIPTION" => GetMessage("DEFAULT_NAV_DESC"),
);
?>