<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">

<?foreach($arResult as $arItem):?>
            <li class="nav__item">
                <a href="<?=$arItem["LINK"]?>" class="link nav__link" title="<?=$arItem["TEXT"]?>"><span class="nav__undescore"><?=$arItem["TEXT"]?></span></a>
            </li>
<?endforeach?>


        </ul>
    </div>
</nav>
<?endif?>