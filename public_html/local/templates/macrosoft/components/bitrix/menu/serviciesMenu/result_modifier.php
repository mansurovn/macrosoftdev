<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

//$arResult['MUSTACHE'] = [];
$arMustache = [];
$needObtainMustache = !empty($arResult);

if ($needObtainMustache) {
    //$arResult = [];

    foreach ($arResult as $itemKey => $arItem) {

        $iconProperty = $arItem['PARAMS']['ICON'];

        $arMustacheItem = [
            'name' => $arItem['TEXT'],
            'icon' => $iconProperty,
            'link' => $arItem["LINK"]
        ];

        if ($itemKey % 2 == 0) {
            $arMustache["col"][0]["item"][] = $arMustacheItem;
        } else {
            $arMustache["col"][1]["item"][] = $arMustacheItem;
        }

    }
    $arResult = [];
    $arResult['MUSTACHE'] = $arMustache;
    unset($arMustache);
}