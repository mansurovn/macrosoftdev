<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);

if ($needObtainMustache) {
    $arResult['NEWS'] = [];

    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        //$iconProperty = $arItem['DISPLAY_PROPERTIES']['ICON'];

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['NAME'],
            'date' => date('d.m', MakeTimeStamp($arItem['ACTIVE_FROM']))
            //'icon' => $iconProperty['VALUE'],
        ];
        $arResult['MUSTACHE']['news'][] = $arMustacheItem;
    }
}