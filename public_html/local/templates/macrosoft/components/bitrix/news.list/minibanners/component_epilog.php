<?php

/**
 * @var array $arParams
 * @var array $templateData
 * @var string $templateFolder
 * @var CBitrixComponent $component
 */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

ZLabs\JSCore::addBlocks(['minibanner']);
