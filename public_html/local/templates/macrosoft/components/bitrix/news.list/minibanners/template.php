<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if ($arResult['ITEMS']) :
    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))
?>

    <div class="minibanner-section">
        <div class="container container_wide">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
                $this->AddDeleteAction(
                    $arItem['ID'],
                    $arItem['DELETE_LINK'],
                    $strElementDelete,
                    $arElementDeleteParams
                );
                $strMainID = $this->GetEditAreaId($arItem['ID']);
                ?>
                    <a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"]?>" id="<?= $strMainID ?>" class="link minibanner" title="<?=$arItem['NAME']?>" style="background-image: url('<?=$arItem['PREVIEW_PICTURE']["SRC"]?>');"></a>
            <?endforeach;?>
        </div>
    </div>
<?php
    endif;
