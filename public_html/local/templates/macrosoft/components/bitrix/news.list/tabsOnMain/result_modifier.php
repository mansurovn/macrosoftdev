<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainSections = !empty($arResult['ITEMS']);
$needObtainMustache = !empty($arResult['ITEMS']);

if ($needObtainSections) {
    //$arSectionIds = [];

    /*foreach ($arResult['ITEMS'] as $arItem) {
        $arSectionIds[] = $arItem['IBLOCK_SECTION_ID'];
    }*/
    $res = CIBlockSection::GetList(
        ['SORT' => 'ACS'],
        array(
            'IBLOCK_ID' => $arParams["IBLOCK_ID"],
            'ACTIVE' => 'Y',
            'GLOBAL_ACTIVE' => 'Y'
        ),
        false,
        ['ID', 'IBLOCK_ID', 'NAME']
    );
    $i=0;
    while ($arSection = $res->Fetch()) {
        foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
            if ($arItem['IBLOCK_SECTION_ID'] === $arSection['ID']) {
                $arSection['ITEMS'][] = $itemKey;
            }
        }
        $arResult['MUSTACHE']['tabs'][] = [
            'id' => $arSection['ID'],
            'name' => $arSection['NAME'],
            'items' => false,
            'activeTab' => $i === 0 ? 'tabs-on-main__tab_active' : '',
            'activeTabPanel' => $i === 0 ? 'tab-panel_active tab-panel_in' : ''
        ];
        $i++;
    }
}
if ($needObtainMustache) {
    $arResult['TABS'] = [];

    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $iconProperty = $arItem['DISPLAY_PROPERTIES']['ICON'];
        $titleProperty = $arItem['DISPLAY_PROPERTIES']['TITLE'];

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['NAME'],
            'icon' => $iconProperty['VALUE'],
            'title' => $titleProperty['~VALUE']['TEXT'] != '' ? $titleProperty['~VALUE']['TEXT'] : $arItem['NAME']
        ];

        foreach ($arResult['MUSTACHE']['tabs'] as &$arTabs) {
            if ($arTabs['id'] == $arItem['IBLOCK_SECTION_ID']) {
                $arTabs['items'][] = $arMustacheItem;
            }
        }
        unset($arTabs);
    }
}