<?php

/**
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 * @var array $arResult
 * @var array $arParams
 * @global CUser $USER
 */

use ZLabs\ServiceFunction;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$arResult['MUSTACHE'] = [];
$needObtainMustache = !empty($arResult['ITEMS']);

if ($needObtainMustache) {
    $arResult['CONTACTS'] = [];

    $strElementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
    $strElementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
    $arElementDeleteParams = array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'));

    foreach ($arResult['ITEMS'] as $itemKey => $arItem) {
        $addressProperty = $arItem['DISPLAY_PROPERTIES']['ADDRESS'];
        $telProperty = $arItem['DISPLAY_PROPERTIES']['TEL'];
        $mapProperty = explode(',', $arItem['DISPLAY_PROPERTIES']['MAP']['VALUE']);
        $bulletProperty = $arItem['DISPLAY_PROPERTIES']['BULLET'];
        $emailProperty = $arItem['DISPLAY_PROPERTIES']['EMAIL'];

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
        $this->AddDeleteAction(
            $arItem['ID'],
            $arItem['DELETE_LINK'],
            $strElementDelete,
            $arElementDeleteParams
        );
        $strMainID = $this->GetEditAreaId($arItem['ID']);

        $arMustacheItem = [
            'strId' => $strMainID,
            'name' => $arItem['NAME'],
            'tel' => $telProperty['VALUE'],
            'email' => $emailProperty['VALUE'],
            'address' => $addressProperty['VALUE'],
            'bullet' => $bulletProperty['DISPLAY_VALUE'],
            'lat' => $mapProperty[0],
            'lon' => $mapProperty[1],
        ];
        $arResult['MUSTACHE']['contacts'][] = $arMustacheItem;
    }
}