<?php
/** @var array $arParams */
/** @var array $arResult */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="text">
    <p><?= $arResult['CONFIG']['text'] ?></p>
</div>
