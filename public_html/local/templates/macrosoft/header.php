<?

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Context;
use \Bitrix\Main\Page\Asset;
use ZLabs\DeferredFunction;

/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 */


if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);
$context = Context::getCurrent();
$request = $context->getRequest();
$server = $context->getServer();
$asset = Asset::getInstance();

?>
<!doctype html>
<html lang="<?= $context->getLanguage() ?>" class="document">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $APPLICATION->ShowTitle() ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=GvkAvkQvXL">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=GvkAvkQvXL">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=GvkAvkQvXL">
    <link rel="manifest" href="/manifest.json?v=GvkAvkQvXL">
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=GvkAvkQvXL" color="#5bbad5">
    <link rel="shortcut icon" href="/favicon.ico?v=GvkAvkQvXL">
    <meta name="theme-color" content="#ffffff">
    <?php
    ZLabs\JSCore::addBlocks(['common']);
    $APPLICATION->ShowHead();
    ?>
</head>
<body class="body">
<?php
    $APPLICATION->ShowPanel();
?>
<?$APPLICATION->IncludeComponent(
	"zlabs:system.empty.cache", 
	"cities.empty", 
	array(
		"CACHE_TIME" => "365100000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "cities.empty",
		"IBLOCK_ID" => "11",
		"GLOBAL_VAR" => "CITIES"
	),
	false
);?>
    <header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <?php if (!CSite::InDir('/index.php')) : ?>
                <a href="/" class="header__brand header__logo" title="<? $APPLICATION->ShowTitle() ?>"></a>
            <?php else : ?>
                <span class="header__brand header__logo" title="<? $APPLICATION->ShowTitle() ?>"></span>
            <?php endif; ?>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "headerTel",
                    Array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "N",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array("", ""),
                        "FILTER_NAME" => "",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "11",
                        "IBLOCK_TYPE" => "content",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "N",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "5",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array("TEL", ""),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "SORT",
                        "SORT_BY2" => "SORT",
                        "SORT_ORDER1" => "ASC",
                        "SORT_ORDER2" => "ASC",
                        "STRICT_SECTION_CHECK" => "N"
                    )
                );?>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_RECURSIVE" => "Y",
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/local/included_areas/header/email.php"
                    )
                );?>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "31536000",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_USE_GROUPS" => "N",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "top_menu"
	),
	false
);?>
    <?echo DeferredFunction::ShowNavChain();
    echo DeferredFunction::ShowH1('h1');?>
    <main class="main <?= DeferredFunction::showWrapperClass('container container_narrow') ?>">
