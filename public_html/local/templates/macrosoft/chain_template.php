<?php

/** @var string $sChainProlog HTML код выводимый перед навигационной цепочкой */
/** @var string $sChainBody HTML код определяющий внешний вид одного пункта навигационной цепочки */
/** @var string $sChainEpilog HTML код выводимый после навигационной цепочки */
/** @var string $strChain HTML код всей навигационной цепочки собранный к моменту подключения шаблона */
/** @var string $TITLE заголовок очередного пункта навигационной цепочки */
/** @var string $LINK ссылка на очередном пункте навигационной цепочки */
/** @var array $arCHAIN копия массива элементов навигационной цепочки */
/** @var array $arCHAIN_LINK ссылка на массив элементов навигационной цепочки */
/** @var int $ITEM_COUNT количество элементов массива навигационной цепочки */
/** @var int $ITEM_INDEX порядковый номер очередного пункта навигационной цепочки */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$sChainProlog = '<div class="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList"><div class="container container_narrow">';
$sChainBody = '';
$sChainEpilog = '</div></div>';

$sChainBody .= (strlen($LINK) > 0 && $ITEM_INDEX != $ITEM_COUNT - 1)
    ? '<a href="' . $LINK . '" class="link breadcrumbs__link icon icon_arrow-right" title="' . htmlspecialchars($TITLE)
    . '" itemprop="name">' . htmlspecialchars($TITLE) . '</a>'
    : '<span class="breadcrumbs__dest" itemprop="name">' . htmlspecialchars($TITLE) . '</span>';
