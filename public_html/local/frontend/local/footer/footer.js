$(document).ready(function () {
    var consulPopUp = $('.footer__consul-form');
    var consulPopUpOpener = $('.footer__consul-opener');

    var feedbackPopUp = $('.footer__feedback-form');
    var feedbackPopUpOpener = $('.footer__feedback-opener');

    if (consulPopUp.length && consulPopUpOpener.length) {
        consulPopUpOpener.fancybox({
            margin: [0, 0],
            gutter: 0,
            btnTpl: {
                smallBtn: '<div class="feedback-form__close icon icon_close"></div>'
            },
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть'
                }
            },
            afterShow: function(){
                consulPopUp.find('.feedback-form__close').on('click', function () {
                    $.fancybox.close(consulPopUpOpener);
                });
            }
        });
    }

    if (feedbackPopUp.length && feedbackPopUpOpener.length) {
        feedbackPopUpOpener.fancybox({
            margin: [0, 0],
            gutter: 0,
            btnTpl: {
                smallBtn: '<div class="feedback-form__close icon icon_close"></div>'
            },
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть'
                }
            },
            afterShow: function(){
                feedbackPopUp.find('.feedback-form__close').on('click', function () {
                    $.fancybox.close(feedbackPopUpOpener);
                });
            }
        });
    }
});