$(document).ready(function () {
    $('.tabs-on-main__tab').on('click', function(){
        var nextPanel = $($(this).attr('href'));
        var tabPanels = nextPanel.closest('.tabs-on-main__tabpanels');

        $('.tabs-on-main__tab').removeClass('tabs-on-main__tab_active');
        $(this).addClass('tabs-on-main__tab_active');

        tabPanels.height(nextPanel.outerHeight());
    });
});