$(document).ready(function () {
    var similarSlider = $('.similar__slider');
    var windowWidth = $(window).width();
    if (similarSlider.length) {
        similarSlider.slick({
            adaptiveHeight: true,
            autoplay: false,
            arrow: true,
            prevArrow: '<button class="similar-slider__arrow icon icon_arrow-left"></button>',
            nextArrow: '<button class="similar-slider__arrow icon icon_arrow-right"></button>',
            appendArrows: $('.similar-slider__toolbar'),
            draggable: true,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: true,
            focusOnSelect: true,
            accessibility: false
        });
        similarSlider.slick('slickSetOption', 'centerPadding', windowWidth > ('1170') ?  ((windowWidth - 1170)/2 + 'px') : 0, true);
        setTimeout

        // событие окончание ресайза
        var resizeTimer;

        $(window).on('resize', function () {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
                windowWidth = $(window).width();
                similarSlider.slick('slickSetOption', 'centerPadding', windowWidth > ('1170') ?  ((windowWidth - 1170)/2 + 'px') : 0, true);
            }, 250);
        });
    }
});