$(document).ready(function () {
    var map = $('.contact-map__canvas');
    var mapLinks = $('.contacts-panel__link');
    var latDefault = mapLinks.eq(0).data('lat');
    var lonDefault = mapLinks.eq(0).data('lon');;

    if (window.ymaps) {
        ymaps.ready(function () {
            map = new ymaps.Map('YMap', {
                center: [latDefault, lonDefault],
                zoom: 17,
                //controls: ['zoomControl', 'searchControl', 'GeolocationControl', 'RouteEditor'],
                controls: [],
                duration: 1000,
                timingFunction: "ease-in",
                flying: true,
                safe: true
            });

            map.controls.add(new ymaps.control.ZoomControl());
            map.controls.add(new ymaps.control.SearchControl());
            map.controls.add(new ymaps.control.GeolocationControl());
            map.controls.add(new ymaps.control.RouteEditor());

            map.behaviors.disable('scrollZoom');

            mapLinks.each(function () {
                placemark = new ymaps.Placemark([$(this).data('lat'), $(this).data('lon')], {
                        hintContent: $(this).data('hintContent'),
                        balloonContent: $(this).data('balloonContent')
                    }
                );
                map.geoObjects.add(placemark);
            });
            $('.contact-map__preloader').fadeOut();
        });

        mapLinks.on('click', function (e) {
            var posX = $(this).data('lat') ? $(this).data('lat') : map.eq(0).data('lat'),
                posY = $(this).data('lon') ? $(this).data('lon') : map.eq(0).data('lon');

            map.setCenter([posX, posY], 17);

            $('html, body').animate({scrollTop: $('.contact-map__canvas').offset().top}, 1000);
            e.preventDefault();
        });
    }
});

function yMapErrorHandler(err) {
    console.log('Error: cannot load Yandex Map');
    console.log(err);
}