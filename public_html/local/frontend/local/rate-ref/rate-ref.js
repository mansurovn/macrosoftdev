$(document).ready(function(){
    var refLinks = $('.rate-ref__link');
    var refAnchor = $('.rate-ref__anchor');
    var refHider = $('.rate-ref__hider');
    var refBlock = $('.rate-ref');

    if (refBlock.length) {
        refLinks.on('click', function(){
            refAnchor.slideToggle();
            $('html, body').delay('300').animate({
                scrollTop: refAnchor.offset().top
            }, 400);

            $('.rate-tabs__tab[href="'+ $(this).attr('href') +'"]').trigger('click');
        });

        refHider.on('click', function(){
            refAnchor.slideUp();
        });
    }
});