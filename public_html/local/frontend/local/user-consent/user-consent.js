;(function () {

    function UserConsentControl(params) {
        this.caller = params.caller;
        this.formNode = params.formNode;
        this.controlNode = params.controlNode;
        this.inputNode = params.inputNode;
        this.config = params.config;
    }

    UserConsentControl.prototype = {};

    BX.UserConsent = {
        msg: {
            'title': 'Согласие пользователя',
            'btnAccept': 'Принимаю',
            'btnReject': 'Не принимаю',
            'loading': 'Загрузка..',
            'errTextLoad': 'Не удалось загрузить текст соглашения.'
        },
        events: {
            'save': 'main-user-consent-request-save',
            'refused': 'main-user-consent-request-refused',
            'accepted': 'main-user-consent-request-accepted'
        },
        textList: {},
        current: null,
        autoSave: false,
        isFormSubmitted: false,
        isConsentSaved: false,
        attributeControl: 'data-bx-user-consent',
        load: function (context) {
            var item = this.find(context)[0];
            if (!item) {
                return null;
            }

            this.bind(item);
            return item;
        },
        loadAll: function (context, limit) {
            this.find(context, limit).forEach(this.bind, this);
        },
        loadFromForms: function () {
            var formNodes = document.getElementsByTagName('FORM');
            formNodes = BX.convert.nodeListToArray(formNodes);
            formNodes.forEach(this.loadAll, this);
        },
        find: function (context) {
            if (!context) {
                return [];
            }

            var controlNodes = context.querySelectorAll('[' + this.attributeControl + ']');
            controlNodes = BX.convert.nodeListToArray(controlNodes);
            return controlNodes.map(this.createItem.bind(this, context)).filter(function (item) {
                return !!item
            });
        },
        bind: function (item) {
            if (item.config.submitEventName) {
                BX.addCustomEvent(item.config.submitEventName, this.onSubmit.bind(this, item));
            }
            else if (item.formNode) {
                BX.bind(item.formNode, 'submit', this.onSubmit.bind(this, item));
            }

            BX.bind(item.controlNode, 'change', this.onChange.bind(this, item));
        },
        onChange: function (item, e) {
            var form = item.formNode;
            form['submit'].disabled = !item.inputNode.checked ? 'disabled' : '';
            e.preventDefault();
        },
        onSubmit: function (item, e) {
            this.isFormSubmitted = true;
            if (this.check(item)) {
                return true;
            }
            else {
                if (e) {
                    e.preventDefault();
                }

                return false;
            }
        },
        createItem: function (context, controlNode) {
            var inputNode = controlNode.querySelector('input[type="checkbox"]');
            if (!inputNode) {
                return;
            }

            try {
                var config = JSON.parse(controlNode.getAttribute(this.attributeControl));
                var parameters = {
                    'formNode': null,
                    'controlNode': controlNode,
                    'inputNode': inputNode,
                    'config': config
                };

                if (context.tagName == 'FORM') {
                    parameters.formNode = context;
                }
                else {
                    parameters.formNode = BX.findParent(inputNode, {tagName: 'FORM'})
                }

                parameters.caller = this;
                return new UserConsentControl(parameters);
            }
            catch (e) {
                return null;
            }
        },
        check: function (item) {
            if (item.inputNode.checked) {
                this.saveConsent(item);
                return true;
            }

            return false;
        },
        setCurrent: function (item) {
            this.current = item;
            this.autoSave = item.config.autoSave;
            this.actionRequestUrl = item.config.actionUrl;
        },
        onAccepted: function () {
            if (!this.current) {
                return;
            }

            var item = this.current;
            this.saveConsent(
                this.current,
                function () {
                    BX.onCustomEvent(item, this.events.accepted, []);
                    BX.onCustomEvent(this, this.events.accepted, [item]);

                    this.isConsentSaved = true;

                    if (this.isFormSubmitted && item.formNode && !item.config.submitEventName) {
                        BX.submit(item.formNode);
                    }
                }
            );

            this.current.inputNode.checked = true;
            this.current = null;
        },
        saveConsent: function (item, callback) {
            this.setCurrent(item);

            var data = {
                'id': item.config.id,
                'sec': item.config.sec,
                'url': window.location.href
            };
            if (item.config.originId) {
                var originId = item.config.originId;
                if (item.formNode && originId.indexOf('%') >= 0) {
                    var inputs = item.formNode.querySelectorAll('input[type="text"], input[type="hidden"]');
                    inputs = BX.convert.nodeListToArray(inputs);
                    inputs.forEach(function (input) {
                        if (!input.name) {
                            return;
                        }
                        originId = originId.replace('%' + input.name + '%', input.value ? input.value : '');
                    });
                }
                data.originId = originId;
            }
            if (item.config.originatorId) {
                data.originatorId = item.config.originatorId;
            }

            BX.onCustomEvent(item, this.events.save, [data]);
            BX.onCustomEvent(this, this.events.save, [item, data]);

            if (this.isConsentSaved || !item.config.autoSave) {
                if (callback) {
                    callback.apply(this, []);
                }
            }
            else {
                this.sendActionRequest(
                    'saveConsent',
                    data,
                    callback,
                    callback
                );
            }
        },
        sendActionRequest: function (action, sendData, callbackSuccess, callbackFailure) {
            callbackSuccess = callbackSuccess || null;
            callbackFailure = callbackFailure || null;

            sendData.action = action;
            sendData.sessid = BX.bitrix_sessid();
            sendData.action = action;

            BX.ajax({
                url: this.actionRequestUrl,
                method: 'POST',
                data: sendData,
                timeout: 10,
                dataType: 'json',
                processData: true,
                onsuccess: BX.proxy(function (data) {
                    data = data || {};
                    if (data.error) {
                        callbackFailure.apply(this, [data]);
                    }
                    else if (callbackSuccess) {
                        callbackSuccess.apply(this, [data]);
                    }
                }, this),
                onfailure: BX.proxy(function () {
                    var data = {'error': true, 'text': ''};
                    if (callbackFailure) {
                        callbackFailure.apply(this, [data]);
                    }
                }, this)
            });
        }
    };

    BX.ready(function () {
        BX.UserConsent.loadFromForms();
    });
})();