$(document).ready(function () {
    var triggers = $('.rate-panel__variant');
    var lists = $('.pop-up__list');

    if (lists.length) {
        triggers.on('click', function(){
            var curList = $(this).find('.pop-up');
            var curTrigger = $(this);
            curList.fadeIn('fast');
            curList.find('.pop-up-list__item').on('click', function(){
                curTrigger.find('.rate-panel-variant__current').text($(this).text());
                curList.fadeOut('fast');
                return false;
            });

            $(this).on('mouseleave', function(){
                curList.fadeOut('fast');
            });
        });

    }
});