$.fn.equality = function () {
    var $containers = $(this);
    var maxHeight = $containers.eq(0).height();
    $containers.each(function () {
        maxHeight = ( $(this).height() > maxHeight ) ? $(this).height() : maxHeight;
    });
    $containers.height(maxHeight);
};

$

$('document').ready(function () {

    var containers = [
        '.tabs-on-main__text',
        '.rate-panel__prices',
        '.rate-panel__duration',
        '.rate-panel__header',
        '.services-block__title'
    ];

    if (containers.length != 0) {
        containers.forEach(function (item, i, array) {
            if ($(item).length != 0) {
                $(item).equality();
            }
        });
    }
});