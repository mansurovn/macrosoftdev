$(document).ready(function () {
    if ($('.review__slider').length != 0) {
        $('.review__slider').slick({
            autoplay: false,
            arrow: true,
            prevArrow: '<button class="review-slider__arrow review-slider__arrow_left icon icon_arrow-left"></button>',
            nextArrow: '<button class="review-slider__arrow review-slider__arrow_right icon icon_arrow-right"></button>',
            appendArrows: $('.review__body'),
            draggable: true,
            infinite: false,
            focusOnSelect: true,
            accessibility: false
        });
    }

    var reviewPopUp = $('.review__form');
    var reviewPopUpOpenLink = $('.review__pop-up-opener');

    if (reviewPopUp.length && reviewPopUpOpenLink.length) {
        reviewPopUpOpenLink.fancybox({
            margin: [0, 0],
            gutter: 0,
            btnTpl: {
                smallBtn: '<div class="feedback-form__close icon icon_close"></div>'
            },
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть'
                }
            },
            afterShow: function(){
                reviewPopUp.find('.feedback-form__close').on('click', function () {
                    $.fancybox.close(reviewPopUpOpenLink);
                });
            }
        });
    }
});
