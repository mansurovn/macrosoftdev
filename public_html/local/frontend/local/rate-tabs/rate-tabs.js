$(document).ready(function(){
    var tablist = $('.rate-tabs__tablist');

    if (tablist.length) {
        var tabs = $('.rate-tabs__tab');

        tabs.on('click', function(){
            var nextPanel = $($(this).attr('href'));
            var tabPanels = nextPanel.closest('.rate-tabs__tabpanels');

            tabs.removeClass('rate-tabs__tab_active');              // переключение стилей вкладок
            $(this).addClass('rate-tabs__tab_active');

            setTimeout(function(){
                tabPanels.height(nextPanel.outerHeight());         // Плавное изменение высоты панелей
            }, 200);

        });




    }
});