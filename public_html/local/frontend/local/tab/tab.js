$(document).ready(function () {
    $('a[role="tab"][data-toggle="tab"]').on('click', function () {
        var nextPanel = $($(this).attr('href'));
        event.preventDefault();
        if (nextPanel.find('.catalog-slider').length != 0) {
            var sliders = nextPanel.find('.catalog-slider');
            sliders.each(function () {
                var slider = $(this);
                setTimeout(function () {
                    slider.slick('setPosition');
                }, 160);
            });
        }
        nextPanel.parent().find('[role="tabpanel"]').each(function () {
            var prevPanel = $(this);
            if (prevPanel.hasClass('tab-panel_active')) {
                prevPanel.removeClass('tab-panel_in');
                setTimeout(function () {
                    prevPanel.removeClass('tab-panel_active');
                }, 150);
            }
        });
        setTimeout(function () {
            nextPanel.addClass('tab-panel_active');
            setTimeout(function () {
                nextPanel.addClass('tab-panel_in');
            }, 20);
        }, 150);
    });
});