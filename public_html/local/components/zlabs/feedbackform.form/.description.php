<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    'NAME' => Loc::getMessage('FEEDBACKFORM_FORM_NAME'),
    'DESCRIPTION' => Loc::getMessage('FEEDBACKFORM_FORM_DESCRIPTION'),
    'SORT' => 30,
    'PATH' => array(
        'ID' => 'zlabs',
        'NAME' => Loc::getMessage('FEEDBACKFORM_FORM_GROUP'),
        'SORT' => 10,
        'CHILD' => array(
            'ID' => 'feedback',
            'NAME' => Loc::getMessage('FEEDBACKFORM_FORM_CHILD_GROUP'),
            'SORT' => 10
        )
    )
);
