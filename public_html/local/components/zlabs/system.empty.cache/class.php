<?php
/**
 * User: Kirill Granin
 * z-labs.ru
 * @ Агенство интернет маркетинга Z-labs - 2016
 */
use \Bitrix\Main\Localization\Loc;

class SystemEmptyCache extends CBitrixComponent
{
    public function executeComponent()
    {
        if ($this->startResultCache()) {
            $this->includeComponentTemplate();
        }
    }
}