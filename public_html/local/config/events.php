<?php

use Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();

/** Init blocks */
$eventManager->addEventHandler('main', 'OnEpilog', [ZLabs\JSCore::class, 'onEpilogHandler']);

/** Get optimal price */
$eventManager->addEventHandler(
    'catalog',
    'OnGetOptimalPrice',
    [ZLabs\Sale\GetOptimalPriceByLocation::class, 'OnGetOptimalPriceHandler']
);

/** Site stub */
$eventManager->addEventHandler('main', 'OnProlog', [ZLabs\SmartSiteStub::class, 'onBitrixPrologHandler']);

$eventManager->addEventHandler('currency', 'CurrencyFormat', [ZLabs\Currency\CurrencyLang::class, 'currencyFormat']);

/**
 * Login is email systems
 */
$eventManager->addEventHandler(
    'main',
    'OnBeforeUserAdd',
    array(ZLabs\User\LoginIsEmail::class, 'onBeforeUserAddHandler')
);
$eventManager->addEventHandler(
    'main',
    'OnBeforeUserRegister',
    array(ZLabs\User\LoginIsEmail::class, 'onBeforeUserAddHandler')
);
/** Comment this before start script normalize login */
$eventManager->addEventHandler(
    'main',
    'OnBeforeUserUpdate',
    array(ZLabs\User\LoginIsEmail::class, 'onBeforeUserAddHandler')
);
$eventManager->addEventHandler(
    'sale',
    'OnOrderNewSendEmail',
    array(ZLabs\User\LoginIsEmail::class, 'onBeforeUserAddHandler')
);
