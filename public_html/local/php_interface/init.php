<?php

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/config/const.php'))
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/config/const.php');

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php'))
    require_once($_SERVER['DOCUMENT_ROOT'] . '/../vendor/autoload.php');

if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/local/config/events.php'))
    require_once($_SERVER['DOCUMENT_ROOT'] . '/local/config/events.php');

//Arrilot\BitrixMigrations\Autocreate\Manager::init($_SERVER['DOCUMENT_ROOT'] . '/../migrations');